# language: fr
Fonctionnalité: Erreurs possible
  En tant qu'utilisateur du service
  Je souhaite avoir un message d'erreur explicite en cas d'erreur
  Afin de corriger mon entrée.

  Scénario: Date de fin avant la date de début
    Étant donné la date de début fixée au '02/01/2021' et une date de fin fixée au '01/01/2021'
    Quand j'appelle le service d'intervalle
    Alors le message d'erreur est 'End date must be after start date'

  Scénario: Date de début erronée
    Étant donné la date de début fixée au '32/12/2020' et une date de fin fixée au '31/01/2021'
    Quand j'appelle le service d'intervalle
    Alors le message d'erreur est 'Start date must be valid'

  Scénario: Date de fin erronée
    Étant donné la date de début fixée au '31/12/2020' et une date de fin fixée au '31/02/2021'
    Quand j'appelle le service d'intervalle
    Alors le message d'erreur est 'End date must be valid'
