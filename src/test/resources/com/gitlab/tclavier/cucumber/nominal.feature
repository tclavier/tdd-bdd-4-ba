# language: fr
Fonctionnalité: Calcul d'intervalle de temps en jours
  En tant qu'utilisateur du service
  Je souhaite donner un date de début et une date de fin
  Afin d'obtenir le nombre de jour qui les sépares.

  Scénario: Une des 2 bornes est incluse
    Étant donné la date de début fixée au '29/12/2020' et une date de fin fixée au '30/12/2020'
    Quand j'appelle le service d'intervalle
    Alors la réponse est de 1

  Scénario: C'est le même jour
    Étant donné la date de début fixée au '26/12/2020' et une date de fin fixée au '26/12/2020'
    Quand j'appelle le service d'intervalle
    Alors la réponse est de 0

  Scénario: À cheval sur 2 mois
    Étant donné la date de début fixée au '30/12/2020' et une date de fin fixée au '6/01/2021'
    Quand j'appelle le service d'intervalle
    Alors la réponse est de 7

  Scénario: Même les années bissextiles
    Étant donné la date de début fixée au '28/02/2020' et une date de fin fixée au '2/03/2020'
    Quand j'appelle le service d'intervalle
    Alors la réponse est de 3