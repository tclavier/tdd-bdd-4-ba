package com.gitlab.tclavier.cucumber;

import com.gitlab.tclavier.api.BetweenXml;
import com.gitlab.tclavier.api.DateXml;
import com.gitlab.tclavier.api.IntervalResource;
import com.gitlab.tclavier.api.IntervalXml;
import io.cucumber.java.fr.Alors;
import io.cucumber.java.fr.Quand;
import io.cucumber.java.fr.Étantdonné;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MyStepdefs {
    BetweenXml betweenXml;
    IntervalXml intervalXml;

    @Étantdonné("la date de début fixée au {string} et une date de fin fixée au {string}")
    public void initBetweenDto(String start, String end) {
        String[] startTokens = start.split("/");
        String[] endTokens = end.split("/");
        betweenXml = BetweenXml.from(
                DateXml.from(startTokens[0], startTokens[1], startTokens[2]),
                DateXml.from(endTokens[0], endTokens[1], endTokens[2]));
    }

    @Quand("j'appelle le service d'intervalle")
    public void callComputeInterval() {
        IntervalResource intervalResource = new IntervalResource();
        intervalXml = intervalResource.computeInterval(betweenXml);
    }

    @Alors("la réponse est de {int}")
    public void answerInDays(int expected) {
        assertEquals(expected, intervalXml.getDays());
    }


    @Alors("le message d'erreur est {string}")
    public void answerWithErrorMessage(String message) {
        assertEquals(message, intervalXml.getErrorMessage());
    }


}
