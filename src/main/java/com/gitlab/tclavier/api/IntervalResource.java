package com.gitlab.tclavier.api;

import com.gitlab.tclavier.domain.IntervalDomain;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import java.security.InvalidParameterException;
import java.time.DateTimeException;
import java.time.LocalDate;

@Path("/interval/")
public class IntervalResource {

    @POST
    public IntervalXml computeInterval(BetweenXml betweenXml) {
        LocalDate startDate;
        try {
            startDate = betweenXml.getStart().toLocalDate();
        } catch (DateTimeException e) {
            return IntervalXml.from("Start date must be valid");
        }

        LocalDate endDate;
        try {
            endDate = betweenXml.getEnd().toLocalDate();
        } catch (DateTimeException e) {
            return IntervalXml.from("End date must be valid");
        }

        try {
            return IntervalXml.from(IntervalDomain.between(startDate, endDate));
        } catch (InvalidParameterException e) {
            return IntervalXml.from("End date must be after start date");
        }
    }
}
