package com.gitlab.tclavier.api;

import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("/v1/")
public class Api extends ResourceConfig {

    public Api() {
        packages("com.gitlab.tclavier.api");
        //register(LoggingFilter.class);
        //register(MultiPartFeature.class);
        //register(AuthFilter.class);
        //register(RolesAllowedDynamicFeature.class);
    }

}
