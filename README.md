# Du BDD et du TDD pour des gens qui ne codent pas ... 

Un flux XML pour décrire deux instants (jour, mois, année) en retour nous avons soit le temps écoulé en jour, soit un message d'erreur.

En entrée :

```xml
<between>
    <start>
        <day></day>
        <month></month>
        <year></year>
    </start>
    <end>
        <day></day>
        <month></month>
        <year></year>
    </end>
</between>
```

En sortie :

```xml
<interval>
    <errorMessage></errorMessage>
    <days></days>
</interval>
```

# Architecture de notre application

```mermaid
sequenceDiagram
    Client ->> Webservice: POST du XML
    Webservice ->> IntervalResource: Transformation du XML en Java
    IntervalResource ->> IntervalResource: Transformation Java proche du XML vers Java métier
    IntervalResource ->> IntervalDomain: Appel avec des paramètres métier
    IntervalDomain ->> IntervalDomain : Calcul métier
    IntervalDomain ->> IntervalResource: Retoune un objet métier
    IntervalResource ->> IntervalResource: Transformation Java métier en Java proche du XML
    IntervalResource ->> Webservice: Transformation en XML
    Webservice ->> Client: Retour
    Note over IntervalDomain: should compute interval between 2 dates
    Note over IntervalDomain: should compute interval for more than one month
    Note over IntervalDomain: should compute interval for more than one year
    Note over IntervalDomain: should throw an exception when endDate is before startDate

    Note over IntervalResource: should take a betweenXml in input and return an intervalXml
    Note over IntervalResource: should have an error message when date are in wrong order
    Note over IntervalResource: should have an error message when start date is invalid
    Note over IntervalResource: should have an error message when end date is invalid

    Note over Webservice: should call web service
```

# Atelier Découverte : Exemple mapping

Voici le résultat de ce qui pourrait être produit par une équipe.

```mermaid
graph LR
    us0[Histoire utilisateur] --- r00[Règle] --- ex00[Example]
    us1[Le calcul est possible] --- r11[Une des 2 bornes est incluse]
    r11 --- ex111[Début=29/12/2020, Fin=30/12/2020, Résultat=1]
    r11 --- ex112[Début=26/12/2020, Fin=26/12/2020, Résultat=0]
    us1 --- r12[On suit le calendrier]
    r12 --- ex121[Début=31/12/2020, Fin=2/2/2021, Résultat=2]
    r12 --- ex122[Début=28/02/2019, Fin=2/03/2019, Résultat=2]
    us1 --- r13[Même les années bissextiles]
    r13 --- ex131[Début=28/02/2020, Fin=2/03/2020, Résultat=3]
    us2[Erreurs possibles] --- r21[Date de fin avant la date de début]
    r21 --- ex211[Début=02/01/2021, Fin=01/01/2021, Message=End date must be after start date]
    us2 --- r22[Date de début erronée]
    r22 --- ex221[Début=32/12/2020, Fin=31/01/2021, Message=Start date must be valid]
    us2 --- r23[Date de fin erronée]
    r23 --- ex231[Début=31/12/2020, Fin=31/02/2021, Message=End date must be valid]
    class us0 us;
    class us1 us;
    class us2 us;
    class r00 rule;
    class r11 rule;
    class r12 rule;
    class r13 rule;
    class r21 rule;
    class r22 rule;
    class r23 rule;
    class ex00 ex;
    class ex111 ex;
    class ex112 ex;
    class ex121 ex;
    class ex122 ex;
    class ex131 ex;
    class ex211 ex;
    class ex221 ex;
    class ex231 ex;
    classDef us fill:#a3be8c;
    classDef rule fill:#d08770;
    classDef ex fill:#b48ead;
```


# Atelier Formulation

Voici le résultat de ce qui pourrait être produit par une équipe :

* [Le calcul est possible](./src/test/resources/com/gitlab/tclavier/cucumber/nominal.feature)
* [Erreurs possibles](./src/test/resources/com/gitlab/tclavier/cucumber/errors.feature)

# Description

Dans cette vidéo : https://video.ploud.fr/videos/watch/23357aee-97a0-490e-b5e4-a35336a987f7 j'essaye de vous montrer comment s’articulent le TDD et le BDD, nous y voyons entre autre comment le TDD fait émerger le code et comment les tests du TDD viennent piocher dans les exemples de l'atelier découverte.

Et le résultat des tests [cucumber est visible en ligne](https://tclavier.gitlab.io/tdd-bdd-4-ba/cucumber.html)

